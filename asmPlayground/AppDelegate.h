//
//  AppDelegate.h
//  asmPlayground
//
//  Created by Mikhail Baynov on 08/09/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
